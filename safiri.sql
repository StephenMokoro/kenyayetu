-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 09, 2018 at 10:17 PM
-- Server version: 5.7.21-0ubuntu0.16.04.1
-- PHP Version: 7.0.25-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `safiri`
--

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `location_auto_id` int(10) NOT NULL,
  `location_name` varchar(100) NOT NULL,
  `location_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`location_auto_id`, `location_name`, `location_date_added`) VALUES
(1, 'Kisumu', '2018-04-09 18:21:16'),
(2, 'Mombasa', '2018-04-09 18:21:16'),
(3, 'Kilifi', '2018-04-09 18:21:30'),
(4, 'Western', '2018-04-09 18:21:30'),
(5, 'Rift Valley', '2018-04-09 20:03:00');

-- --------------------------------------------------------

--
-- Table structure for table `question_types`
--

CREATE TABLE `question_types` (
  `question_type_auto_id` int(10) NOT NULL,
  `question_type_name` varchar(50) NOT NULL,
  `question_type_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_types`
--

INSERT INTO `question_types` (`question_type_auto_id`, `question_type_name`, `question_type_date_added`) VALUES
(1, 'Entrance Fees', '2018-04-09 18:25:36'),
(2, 'Location', '2018-04-09 18:25:36'),
(3, 'Specific', '2018-04-09 19:11:04');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE `sites` (
  `site_auto_id` int(10) NOT NULL,
  `site_name` varchar(150) NOT NULL,
  `site_category_id` int(10) NOT NULL,
  `site_location_id` int(10) NOT NULL,
  `site_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_auto_id`, `site_name`, `site_category_id`, `site_location_id`, `site_date_added`) VALUES
(1, 'Mount Kenya', 3, 1, '2018-04-09 19:53:57'),
(2, 'Mount Longonot', 3, 5, '2018-04-09 20:16:29');

-- --------------------------------------------------------

--
-- Table structure for table `site_categories`
--

CREATE TABLE `site_categories` (
  `category_auto_id` int(10) NOT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_caption` varchar(50) DEFAULT NULL,
  `category_info` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_categories`
--

INSERT INTO `site_categories` (`category_auto_id`, `category_name`, `category_caption`, `category_info`) VALUES
(1, 'Nature Trails', 'Amazing Nature Trails just for you', NULL),
(2, 'Culture Visits', 'Another one', NULL),
(3, 'Mountain Climbing', 'And another', NULL),
(4, 'Site seeing', 'Am last', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_questions`
--

CREATE TABLE `site_questions` (
  `site_q_auto_id` int(10) NOT NULL,
  `site_question` varchar(100) DEFAULT NULL,
  `site_category_id` int(10) DEFAULT NULL,
  `site_q_type_id` int(10) DEFAULT NULL,
  `site_q_anwer_type_id` int(10) DEFAULT NULL,
  `site_q_date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_questions`
--

INSERT INTO `site_questions` (`site_q_auto_id`, `site_question`, `site_category_id`, `site_q_type_id`, `site_q_anwer_type_id`, `site_q_date_added`) VALUES
(1, 'What location are you interested in?', 2, 2, 1, '2018-04-09 19:25:58'),
(2, 'Which Mountain would you like to climb?', 3, 3, NULL, '2018-04-09 19:13:03');

-- --------------------------------------------------------

--
-- Table structure for table `tourists`
--

CREATE TABLE `tourists` (
  `tourist_auto_id` int(10) NOT NULL,
  `tourist_fname` varchar(30) NOT NULL,
  `toursit_lname` varchar(30) NOT NULL,
  `tourist_country` varchar(30) NOT NULL,
  `tourist_email` varchar(100) NOT NULL,
  `tourist_password` varchar(200) NOT NULL,
  `tourist_date_registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tourists`
--

INSERT INTO `tourists` (`tourist_auto_id`, `tourist_fname`, `toursit_lname`, `tourist_country`, `tourist_email`, `tourist_password`, `tourist_date_registered`) VALUES
(1, 'Emily', 'Karinge', 'KENYA', 'ekaringe@gmail.com', 'c9741f832b06c7f2f1d154b8534e2e32', '2018-03-28 19:17:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_auto_id`);

--
-- Indexes for table `question_types`
--
ALTER TABLE `question_types`
  ADD PRIMARY KEY (`question_type_auto_id`);

--
-- Indexes for table `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`site_auto_id`),
  ADD KEY `site_location_id_fk` (`site_location_id`),
  ADD KEY `site_category_id_fk` (`site_category_id`);

--
-- Indexes for table `site_categories`
--
ALTER TABLE `site_categories`
  ADD PRIMARY KEY (`category_auto_id`);

--
-- Indexes for table `site_questions`
--
ALTER TABLE `site_questions`
  ADD PRIMARY KEY (`site_q_auto_id`),
  ADD KEY `question_site_category_id_fk` (`site_category_id`),
  ADD KEY `question_site_q_type_id_fk` (`site_q_type_id`);

--
-- Indexes for table `tourists`
--
ALTER TABLE `tourists`
  ADD PRIMARY KEY (`tourist_auto_id`),
  ADD UNIQUE KEY `tourist_email` (`tourist_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `location_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `question_types`
--
ALTER TABLE `question_types`
  MODIFY `question_type_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sites`
--
ALTER TABLE `sites`
  MODIFY `site_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `site_categories`
--
ALTER TABLE `site_categories`
  MODIFY `category_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_questions`
--
ALTER TABLE `site_questions`
  MODIFY `site_q_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tourists`
--
ALTER TABLE `tourists`
  MODIFY `tourist_auto_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `site_location_id_fk` FOREIGN KEY (`site_location_id`) REFERENCES `locations` (`location_auto_id`),
  ADD CONSTRAINT `sites_category_id_fk` FOREIGN KEY (`site_category_id`) REFERENCES `site_categories` (`category_auto_id`);

--
-- Constraints for table `site_questions`
--
ALTER TABLE `site_questions`
  ADD CONSTRAINT `question_site_category_id_fk` FOREIGN KEY (`site_category_id`) REFERENCES `site_categories` (`category_auto_id`),
  ADD CONSTRAINT `question_site_q_type_id_fk` FOREIGN KEY (`site_q_type_id`) REFERENCES `question_types` (`question_type_auto_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
