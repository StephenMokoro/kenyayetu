<?php
session_start();

?>
<!DOCTYPE html>
<html>
<head>
	<title>Kenya Yetu</title>
	<link rel="stylesheet" type="text/css" href="../assets/css/home.css">
	<link rel="stylesheet" type="text/css" href="../assets/bootstrap-3.3.7/dist/css/bootstrap.min.css">
</head>

<body class="main">

<div class="login-screen"></div>
    <div class="login-center">
        <div class="container min-height" style="margin-top: 20px;">
        	<div class="row">
                <div class="col-xs-4 col-md-offset-8">
                    <div class="login" id="card">
                    	<div class="front signin_form"> 
                        <p>Login Your Account</p>
                        
                         <?php if(isset($_SESSION['loginsuccess'])||isset($_SESSION['loginsuccess']))
                            {   

                                $success = $_SESSION['loginsuccess'];
                                $fail = $_SESSION['loginfail'];

                                if ($success!="" && $fail==""){ echo '
                                <div class="messagebox alert alert-success" style="display: block">
                                        <button type="button" class="close" data-dismiss="alert">*</button>
                                        <div class="cs-text">
                                            <i class="fa fa-check"></i>
                                            <strong><span>';echo $success; echo '</span></strong>
                                        </div> 
                                </div>';}else if($success=="" && $fail!=""){ echo '
                                <div class="messagebox alert alert-danger" style="display: block">
                                        <button type="button" class="close" data-dismiss="alert">*</button>
                                        <div class="cs-text">
                                            <i class="fa fa-close"></i>
                                            <strong><span>';echo $fail; echo '</span></strong>
                                        </div> 
                                </div>';}else if($success=="" && $fail==""){ echo '<div></div>';}
                              }else{
                                 echo '<div></div>';
                              } ?>
                          <form class="login-form" method="post" action="http://localhost/kenyayetu/connections/login.php">
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="email" class="form-control" placeholder="Email" name="username">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-user"></i>
                                      </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="password" class="form-control" placeholder="Password" name="password">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-lock"></i>
                                      </span>
                                  </div>
                              </div>
                             <!--  <div class="checkbox">
                              <label><input type="checkbox">Remember me next time.</label>
                              </div> -->
                              
                              <div class="form-group sign-btn">
                                  <input type="submit" class="btn" value="Log in">
                                  <br><br><a href="#" id="flip-btn" class="signup signup_link">Create New Account</a></p>
                              </div>
                          </form>
                        </div>
                        <div class="back signup_form" style="opacity: 0;"> 
                          <p>Sign Up for Your New Account</p>
                          <form class="login-form">
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="email" class="form-control" placeholder="Email">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-envelope"></i>
                                      </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="password" class="form-control" placeholder="Password">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-lock"></i>
                                      </span>
                                  </div>
                              </div>
                              <div class="form-group">
                                  <div class="input-group">
                                      <input type="password" class="form-control" placeholder="Confirm Password">
                                      <span class="input-group-addon">
                                          <i class="glyphicon glyphicon-check"></i>
                                      </span>
                                  </div>
                              </div>
                              <div class="form-group sign-btn">
                                  <input type="submit" class="btn" value="Sign up">
                                  <br><br>
                                  <p>Already have an account? <a href="#" id="unflip-btn" class="signup">Log in</a></p>
                              </div>
                          </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/js/jquery-3.3.1/jquery-3.3.1.min.js"></script>
    <script src="../assets/js/jQuery-Flip/1.0.18/jquery.flip.js"></script>
    <script src="../assets/bootstrap-3.3.7/dist/js/bootstrap.min.js"></script>
    <script>
      $().ready(function() {
        $("#card").flip({
          trigger: 'manual'
        });
      });


      $(".signup_link").click(function() {

        $(".signin_form").css('opacity', '0');
        $(".signup_form").css('opacity', '100');
        
        
        $("#card").flip(true);
        
        return false;
      });

      $("#unflip-btn").click(function(){
        
        $(".signin_form").css('opacity', '100');
        $(".signup_form").css('opacity', '0');
        
          $("#card").flip(false);
        
        return false;
        
      });
    </script>
    
  </body>
</html>