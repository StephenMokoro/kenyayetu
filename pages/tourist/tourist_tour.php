<?php 
session_start();
if($_SESSION['loginsuccess'] !="")//check if login session exists [created at login]
{
    $_SESSION['loginfail']='';//if yes, keep login fail empty.

    $conn = mysqli_connect('localhost', 'root', 'WiMm@!2018', 'safiri');//this is ok
  
    //query string to select all site_categories from db
    $siteCategoriesQuery="SELECT * FROM site_categories";

    //query the db with mysqli
    $site_categories=mysqli_query($conn,$siteCategoriesQuery);//array of site categories

    $locationsQuery=mysqli_query($conn,"SELECT * FROM locations");//array of locations



}else{//if login session does not exist, set session expire 
        $_SESSION['loginfail']='Session expired.';
        $_SESSION['loginsuccess']='';
    echo "<script type='text/javascript'>location.href='http://localhost/kenyayetu/pages/home.php';</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="http://localhost/kenyayetu/assets/adminto/images/favicon.ico">
    <title>KENYA YETU</title>
    <link href="http://localhost/kenyayetu/assets/css/tourist/tour_page.css" rel="stylesheet" type="text/css" />
    <?php include '../header_links.php';?>


</style>
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>Kenya Yetu<span></span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Explore Kenya</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <?php include '../tourist/navigation.php';?>


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                        <!--     Fonts and icons     -->
                        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
                        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
                   
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <div class="section-title text-center wow zoomIn">
                                        <h1>Frequently Asked Questions</h1>
                                        <span></span>
                                        <p>Our Frequently Asked Questions here.</p>
                                    </div>
                                </div>
                            </div> -->
                            <div class="row">               
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <?php if(!empty($site_categories)){
                                    //loop through the returned category and display each
                                    $count=0;
                                    foreach($site_categories as $category)
                                    {
                                        $category_id=$category['category_auto_id'];//get category id and use it to count no. of sites added under each category
                                        $query="SELECT * FROM site_questions WHERE site_category_id='$category_id'";
                                        $site_questions=mysqli_query($conn,$query);



                                        ?>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="heading_<?php echo $count;?>">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapse_<?php echo $count;?>" aria-expanded="true" aria-controls="collapse_<?php echo $count;?>">
                                                        <?php echo $category['category_name'];?> 
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse_<?php echo $count;?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $count;?>">
                                                <div class="panel-body">
                                                <?php if(!empty($site_questions))
                                                { 

                                                    foreach($site_questions as $site_q)
                                                    {
                                                        $question_type_id=$site_q['site_q_type_id'];
                                                        if($question_type_id=='2')
                                                        {
                                                            echo '<p>'.$site_q['site_question'].'</p><br>';

                                                            if(!empty($locationsQuery))
                                                            {
                                                                foreach($locationsQuery as $location)
                                                                {                                             echo '<input type="radio" name="location">'.' '.$location['location_name'].'<br>';
                                                                }
                                                            }
                                                        }else if($question_type_id=='3' && $category_id=='3' )
                                                            {
                                                                 echo '<p>'.$site_q['site_question'].'</p>';
                                                                 $mountainsQuery=mysqli_query($conn,"SELECT * FROM sites WHERE site_category_id='3'");//array of mountains
                                                                 if(!empty($mountainsQuery))
                                                                    {

                                                                        foreach($mountainsQuery as $mts)
                                                                        {   echo '<input type="radio" name="mountains">'.' '.$mts['site_name'].'<br>';
                                                                        }
                                                                    }

                                                            }else{
                                                                    echo '<p>'.$site_q['site_question'].'</p><br>';
                                                                    
                                                            }



                                                        {
                                                                
                                                            }

                                                        
                                                    
                                                        
                                                     }
                                                 } ?>
                                                </div>
                                            </div>
                                        </div>
                                       <?php $count=$count+1;
                                   } 

                                   } ?>
                                    </div>
                                </div><!--- END COL -->     
                            </div><!--- END ROW -->         
                        </div>
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Emily Karinge
                </footer>

            </div>

        </div>
        <!-- END wrapper -->
    <?php include '../footer_links.php';?>

    </body>
    <script>
        $(window).load(function() {
            $('#welcomeModal').modal('show');
        });
    (function($) {
        'use strict';
        
        jQuery(document).on('ready', function(){
        
                $('a.page-scroll').on('click', function(e){
                    var anchor = $(this);
                    $('html, body').stop().animate({
                        scrollTop: $(anchor.attr('href')).offset().top - 50
                    }, 1500);
                    e.preventDefault();
                });     

        });     
    })(jQuery);


  


    </script>

<!-- Mirrored from coderthemes.com/adminto/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Mar 2016 06:34:31 GMT -->
</html>