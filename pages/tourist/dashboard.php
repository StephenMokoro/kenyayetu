<?php 
session_start();
if($_SESSION['loginsuccess'] !="")//check if login session exists [created at login]
{
    $_SESSION['loginfail']='';//if yes, keep login fail empty.

    $conn = mysqli_connect('localhost', 'root', 'WiMm@!2018', 'safiri');//this is ok
  
    //query string to select all site_categories from db
    $siteCategoriesQuery="SELECT * FROM site_categories";

    //query the db with mysqli
    $site_categories=mysqli_query($conn,$siteCategoriesQuery);//array of site categories
}else{//if login session does not exist, set session expire 
        $_SESSION['loginfail']='Session expired.';
        $_SESSION['loginsuccess']='';
    echo "<script type='text/javascript'>location.href='http://localhost/kenyayetu/pages/home.php';</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="http://localhost/kenyayetu/assets/adminto/images/favicon.ico">

    <title>KENYA YETU</title>
    <?php include '../header_links.php';?>
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <a href="index.html" class="logo"><span>Kenya Yetu<span></span></span><i class="zmdi zmdi-layers"></i></a>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">

                        <!-- Page title -->
                        <ul class="nav navbar-nav navbar-left">
                            <li>
                                <button class="button-menu-mobile open-left">
                                    <i class="zmdi zmdi-menu"></i>
                                </button>
                            </li>
                            <li>
                                <h4 class="page-title">Dashboard</h4>
                            </li>
                        </ul>

                        <!-- Right(Notification and Searchbox -->
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <!-- Notification -->
                                <div class="notification-box">
                                    <ul class="list-inline m-b-0">
                                        <li>
                                            <a href="javascript:void(0);" class="right-bar-toggle">
                                                <i class="zmdi zmdi-notifications-none"></i>
                                            </a>
                                            <div class="noti-dot">
                                                <span class="dot"></span>
                                                <span class="pulse"></span>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- End Notification bar -->
                            </li>
                            <li class="hidden-xs">
                                <form role="search" class="app-search">
                                    <input type="text" placeholder="Search..."
                                           class="form-control">
                                    <a href="#"><i class="fa fa-search"></i></a>
                                </form>
                            </li>
                        </ul>

                    </div><!-- end container -->
                </div><!-- end navbar -->
            </div>
            <!-- Top Bar End -->


            <?php include '../tourist/navigation.php';?>


            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                        <div class="row">

                            <!-- button to load tour modal -->
                            <div class="col-lg-12 col-md-12">
                                <button class="btn btn-success" id="take_a_tour" onclick="showModal();">Take a Tour</button>
                                <br><br>
                            </div>


                        <?php if(!empty($site_categories)){
                            //loop through the returned priests and display each
                        foreach($site_categories as $category)
                            {
                                $category_id=$category['category_auto_id'];//get category id and use it to count no. of sites added under each category
                                $sites_count_query="SELECT site_category_id FROM sites WHERE site_category_id='$category_id'";
                                $count_number=mysqli_num_rows(mysqli_query($conn,$sites_count_query));

                                ?>
                            <div class="col-lg-4 col-md-6">
                                <div class="card-box">
                                    <h4 class="header-title m-t-0 m-b-30"><?php echo $category['category_name'];?></h4>
                                    <div class="widget-chart-1">
                                        <div class="widget-chart-box-1">
                                        <p><?php echo $category['category_caption'];?></p>
                                        </div>
                                        <div class="widget-detail-1">
                                            <h2 class="p-t-10 m-b-0"> <?php echo $count_number;?> </h2>
                                            <p class="text-muted">Total </p>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end col -->
                            <?php }}?>
                        </div><!--row-->
                            
                    </div> <!-- container -->
                </div> <!-- content -->

                <footer class="footer text-right">
                    2018 © Emily Karinge
                </footer>

            </div>

        </div>
        <!-- END wrapper -->


        <input type="text" id="modal_session" value="<?php echo $_SESSION['welcome_modal'];?>" hidden="true"><!-- input to store modal_show session used to prevent reload of modal form everytime you return to dashboard -->



        <div class="modal fade" id="welcomeModal">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: #800000;font-weight: bolder;"><b>Welcome <?php echo $_SESSION['tourist_name']; ?>!</b></h4>
                  </div>
                  <div >
                    <h4>New to Kenya? Relax. We will suggest amazing places to visit</h4>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-success pull-right" href="http://localhost/kenyayetu/pages/tourist/tourist_tour.php">Take a Tour</a>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <div class="modal fade" id="tourModal">
          <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" style="color: #800000;font-weight: bolder;"><b>Awesome <?php echo $_SESSION['tourist_name']; ?>!</b></h4>
                  </div>
                  <div >
                    <h4>Hope you are having a good time. Let's take you around</h4>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-success pull-right" href="http://localhost/kenyayetu/pages/tourist/tourist_tour.php">Proceed</a>
                    <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                  </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <?php include '../footer_links.php';?>
    </body>
    <script>
        $(window).load(function() {
            var show_modal_session=$('#modal_session').val();
            if(show_modal_session!="")
            {
                $('#welcomeModal').modal('show');
            }else{
                $('#welcomeModal').modal('hide');
            }
        });
        function showModal()
        {
            $('#tourModal').modal('show');//show modal when function is clicked.
        }
    </script>

<!-- Mirrored from coderthemes.com/adminto/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 09 Mar 2016 06:34:31 GMT -->
</html>