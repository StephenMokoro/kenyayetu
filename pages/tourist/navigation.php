<!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!-- User -->
                    <div class="user-box">
                        <div class="user-img">
                            <img src="http://localhost/kenyayetu/assets/images/profile_holder.png" alt="user-img" title="Mat Helme" class="img-circle img-thumbnail img-responsive">
                            <div class="user-status offline"><i class="zmdi zmdi-dot-circle"></i></div>
                        </div>
                        <h5><a href="#"><?php echo $_SESSION['tourist_name']; ?></a> </h5>
                       <!--  <ul class="list-inline">
                            <li>
                                <a href="#" >
                                    <i class="zmdi zmdi-settings"></i>
                                </a>
                            </li>

                            <li>
                                <a href="#" class="text-custom">
                                    <i class="zmdi zmdi-power"></i>
                                </a>
                            </li>
                        </ul> -->
                    </div>
                    <!-- End User -->

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                            <li class="text-muted menu-title">Navigation</li>

                            <li>
                                <a href="http://localhost/kenyayetu/pages/tourist/dashboard.php" class="waves-effect active"><i class="zmdi zmdi-view-dashboard"></i> <span> Dashboard </span> </a>
                            </li>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-invert-colors"></i> <span> Activities </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="#">Site Seeing</a></li>
                                    <li><a href="#">Nature Trails</a></li>
                                    <li><a href="#">Mountains Climbing</a></li>
                                    <li><a href="#">Culture Visits</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-view-list"></i> <span> Accommodation </span> <span class="menu-arrow"></span></a>
                                 <ul class="list-unstyled">
                                    <li><a href="#">Classic Hotels</a></li>
                                    <li><a href="#">Resorts</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="zmdi zmdi-chart"></i><span> Learn Swahili </span> <span class="label label-purple pull-right">New</span></a>
                                <ul class="list-unstyled">
                                    <li><a href="#">Basic</a></li>
                                    <li><a href="#">Advanced</a></li>
                                </ul>
                            </li>
                            <li class="has_sub" style="background-color: #839192;">
                                <a href="http://localhost/kenyayetu/connections/logout.php" style="color: #FFFFFF;"><i class="fa fa-sign-out"></i><span> Sign Out </span> </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->
